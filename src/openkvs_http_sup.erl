%% Copyright 2016, Bernard Notarianni
%%
%% Licensed under the Apache License, Version 2.0 (the "License"); you may not
%% use this file except in compliance with the License. You may obtain a copy of
%% the License at
%%
%%   http://www.apache.org/licenses/LICENSE-2.0
%%
%% Unless required by applicable law or agreed to in writing, software
%% distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
%% WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
%% License for the specific language governing permissions and limitations under
%% the License.

%%%-------------------------------------------------------------------
%% @doc openkvs_http top level supervisor.
%% @end
%%%-------------------------------------------------------------------

-module(openkvs_http_sup).
-author("Bernard Notarianni").

-behaviour(supervisor).

%% API
-export([start_link/0]).

%% Supervisor callbacks
-export([init/1]).

-define(SERVER, ?MODULE).

%%====================================================================
%% API functions
%%====================================================================

start_link() ->
  supervisor:start_link({local, ?SERVER}, ?MODULE, []).

%%====================================================================
%% Supervisor callbacks
%%====================================================================

init(_Args) ->
  Cache = {local, openkvs_connections_cache},
  CacheOpts = [{max_objs, 50}],

  ObjectCacheSpecs =
    #{id => openkvs_connections_cache,
      start => {lru, start_link, [Cache, CacheOpts]},
      restart => permanent,
      shutdown => 2000,
      type => worker,
      modules => [lru]
     },

  OpenkvsHttpSpecs =
    #{id => openkvs_http,
      start => {openkvs_http, start_link, []}},

  SupFlags = #{strategy => one_for_one, intensity => 5, period => 10},
  {ok, {SupFlags, [OpenkvsHttpSpecs, ObjectCacheSpecs]}}.

