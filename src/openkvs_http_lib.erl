%% Copyright 2016, Bernard Notarianni
%%
%% Licensed under the Apache License, Version 2.0 (the "License"); you may not
%% use this file except in compliance with the License. You may obtain a copy of
%% the License at
%%
%%   http://www.apache.org/licenses/LICENSE-2.0
%%
%% Unless required by applicable law or agreed to in writing, software
%% distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
%% WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
%% License for the specific language governing permissions and limitations under
%% the License.

-module(openkvs_http_lib).
-author("Bernard Notarianni").

-export([reply_doc/3]).
-export([reply_json/3]).
-export([reply_code/3]).
-export([connect/1]).

%% =============================================================================
%% Reply helpers
%% =============================================================================

reply_doc(Doc, Req, State ) ->
  Json = jsx:encode(Doc),
  reply_json(Json, Req, State).

reply_json(Json, Req, State) ->
  Headers = [{<<"content-type">>, <<"application/json">>}],
  reply(200, Headers, Json, Req, State).

reply_code(HttpCode, Req, State ) ->
  reply(HttpCode, [], [], Req, State).

reply(HttpCode, Headers, Content, Req, State) ->
  H = [{<<"server">>, <<"BarrelDB (Erlang/OTP)">>} | Headers],
  {ok, Req2} = cowboy_req:reply(HttpCode, H, Content, Req),
  {ok, Req2, State}.

%% =============================================================================
%% Openkvs helpers
%% =============================================================================

connect(Url) ->
  Cache = openkvs_connections_cache,
  Conn = case lru:get(Cache, Url) of
           undefined ->
             {ok, C} =  openkvs:connect(Url),
             lru:add(Cache, Url, C),
             lru:get(Cache, Url);
           Value ->
             Value
         end,
  {ok, Conn}.
