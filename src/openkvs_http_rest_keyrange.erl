%% Copyright 2016, Bernard Notarianni
%%
%% Licensed under the Apache License, Version 2.0 (the "License"); you may not
%% use this file except in compliance with the License. You may obtain a copy of
%% the License at
%%
%%   http://www.apache.org/licenses/LICENSE-2.0
%%
%% Unless required by applicable law or agreed to in writing, software
%% distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
%% WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
%% License for the specific language governing permissions and limitations under
%% the License.

-module(openkvs_http_rest_keyrange).
-author("Bernard Notarianni").


%% API
-export([init/3]).
-export([rest_init/2]).

-export([allowed_methods/2]).
-export([content_types_provided/2]).
-export([resource_exists/2]).
-export([to_json/2]).

-export([trails/0]).

trails() ->
  Metadata =
    #{ get => #{ summary => "Get list of all available documents."
               , produces => ["application/json"]
               , parameters =>
                   [#{ name => <<"dbid">>
                     , description => <<"Database ID">>
                     , in => <<"path">>
                     , required => true
                     , type => <<"string">>}
                   ]
               }
     },
  [trails:trail("/_keyrange/:startkey/:endkey", ?MODULE, [], Metadata)].

%% =============================================================================
%% Cowboy Rest callbacks
%% =============================================================================

init(_, _, _) -> {upgrade, protocol, cowboy_rest}.

rest_init(Req, _) -> {ok, Req, #{}}.

allowed_methods(Req, State) ->
  Methods = [<<"HEAD">>, <<"OPTIONS">>, <<"GET">>],
  {Methods, Req, State}.

content_types_provided(Req, State) ->
  CTypes = [{{<<"application">>, <<"octet-stream">>, []}, to_json}],
  {CTypes, Req, State}.


resource_exists(Req, _State) ->
  {StartKey, Req2} = cowboy_req:binding(startkey, Req, 0),
  {EndKey, Req3} = cowboy_req:binding(endkey, Req2, 10),
  {UriBin, Req4} = cowboy_req:cookie(<<"storage">>, Req3),
  {StartIncl, Req5} = qs_boolean(<<"startkey_inclusive">>, Req4, true),
  {EndIncl, Req6} = qs_boolean(<<"endkey_inclusive">>, Req5, true),
  {Max, Req7} = cowboy_req:qs_val(<<"max">>, Req6, 10),
  Uri = binary_to_list(UriBin),
  {ok, Conn} = openkvs_http_lib:connect(Uri),
  Range = openkvs:get_keyrange(Conn, StartKey, EndKey, Max,
                               [{startkey_inclusive, StartIncl},
                                {endkey_inclusive, EndIncl}]),
  {true, Req7, Range}.


to_json(Req, State) ->
  Range = State,
  Json = [#{<<"key">> => K, <<"value">> => V, <<"version">> => Ver}
          || {K, {V, Ver}} <- Range],
  Value = jsx:encode(Json),
  {Value, Req, State}.

%% =============================================================================
%% Internals
%% =============================================================================

qs_boolean(Param, Req, Default) ->
  case cowboy_req:qs_val(Param, Req) of
    {<<"true">>,  Req2} -> {true, Req2};
    {<<"false">>, Req2} -> {false, Req2};
    {_, Req2} -> {Default, Req2}
  end.
