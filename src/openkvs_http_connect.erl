%% Copyright 2016, Bernard Notarianni
%%
%% Licensed under the Apache License, Version 2.0 (the "License"); you may not
%% use this file except in compliance with the License. You may obtain a copy of
%% the License at
%%
%%   http://www.apache.org/licenses/LICENSE-2.0
%%
%% Unless required by applicable law or agreed to in writing, software
%% distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
%% WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
%% License for the specific language governing permissions and limitations under
%% the License.

-module(openkvs_http_connect).
-author("Bernard Notarianni").


%% API
-export([init/3]).
-export([rest_init/2]).

-export([allowed_methods/2]).
-export([content_types_accepted/2]).
-export([create_connect/2]).

-export([trails/0]).

trails() ->
  Metadata =
    #{ post => #{ summary => "Connect to a storage"
                , produces => ["application/json"]
                , parameters =>
                    [#{ name => <<"dbid">>
                      , description => <<"Database ID">>
                      , in => <<"path">>
                      , required => true
                      , type => <<"string">>}
                    ]
               }
     },
  [trails:trail("/_connect", ?MODULE, [], Metadata)].


init(_, _, _) -> {upgrade, protocol, cowboy_rest}.

rest_init(Req, _) ->
  {ok, Req, #{}}.

allowed_methods(Req, State) ->
  Methods = [<<"HEAD">>, <<"OPTIONS">>, <<"POST">>, <<"DELETE">>],
  {Methods, Req, State}.

content_types_accepted(Req, State) ->
	{[{{<<"application">>, <<"json">>, []}, create_connect}], Req, State}.


create_connect(Req, State) ->
  {Method, Req2} = cowboy_req:method(Req),
  connect_to_json(Method, Req2, State).

connect_to_json(<<"POST">>, Req, State) ->
  {ok, Body, Req2} = cowboy_req:body(Req),
  Json = jsx:decode(Body, [return_maps]),
  UriBin = maps:get(<<"uri">>, Json),
  Uri = binary_to_list(UriBin),
  reply_json(openkvs_http_lib:connect(Uri), Uri, Req2, State);

connect_to_json(<<"DELETE">>, Req, State) ->
  Req2 = cowboy_req:set_resp_cookie(<<"storage">>, <<>>, [], Req),
  Reply = #{ disconnected => true},
  {{true, Reply}, Req2, State};

connect_to_json(_Method, Req, State) ->
  openkvs_http_lib:reply_code(405, Req, State).



reply_json({error, not_found}, _, Req, State) ->
  {false, Req ,State};

reply_json({ok, _Conn}, Uri, Req, State) ->
  Req2 = cowboy_req:set_resp_cookie(<<"storage">>, Uri, [], Req),
  Req3 = cowboy_req:set_resp_body(Uri, Req2),
  {true, Req3, State}.


