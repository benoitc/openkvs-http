%% Copyright 2016, Bernard Notarianni
%%
%% Licensed under the Apache License, Version 2.0 (the "License"); you may not
%% use this file except in compliance with the License. You may obtain a copy of
%% the License at
%%
%%   http://www.apache.org/licenses/LICENSE-2.0
%%
%% Unless required by applicable law or agreed to in writing, software
%% distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
%% WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
%% License for the specific language governing permissions and limitations under
%% the License.

-module(openkvs_http_rest).
-author("Bernard Notarianni").


%% API
-export([init/3]).
-export([rest_init/2]).

-export([allowed_methods/2]).
-export([content_types_provided/2]).
-export([content_types_accepted/2]).
-export([resource_exists/2]).
-export([delete_resource/2]).

-export([generate_etag/2]).
-export([to_octet_stream/2]).
-export([from_octet_stream/2]).

-export([trails/0]).

trails() ->
  Metadata =
    #{ get => #{ summary => "Get list of all available documents."
               , produces => ["application/json"]
               , parameters =>
                   [#{ name => <<"dbid">>
                     , description => <<"Database ID">>
                     , in => <<"path">>
                     , required => true
                     , type => <<"string">>}
                   ]
               }
     },
  [trails:trail("/:key", ?MODULE, [], Metadata)].


-record(state, {method, storage, uri, conn, key, value, revision}).

init(_, _, _) -> {upgrade, protocol, cowboy_rest}.

rest_init(Req, _) -> {ok, Req, #state{}}.

allowed_methods(Req, State) ->
  Methods = [<<"HEAD">>, <<"OPTIONS">>, <<"GET">> , <<"PUT">>, <<"DELETE">>],
  {Methods, Req, State}.

content_types_provided(Req, State) ->
  CTypes = [{{<<"application">>, <<"octet-stream">>, []}, to_octet_stream}],
  {CTypes, Req, State}.

content_types_accepted(Req, State) ->
	{[{{<<"application">>, <<"octet-stream">>, []}, from_octet_stream}],
   Req, State}.

resource_exists(Req, State) ->
  {Method, Req2} = cowboy_req:method(Req),
  {Key, Req3} = cowboy_req:binding(key, Req2),
  {UriBin, Req4} = cowboy_req:cookie(<<"storage">>, Req3),
  Uri = binary_to_list(UriBin),
  {ok, Conn} = openkvs_http_lib:connect(Uri),
  State2 = State#state{method=Method, uri=Uri, conn=Conn, key=Key},
  case openkvs:get(Conn, Key) of
    {error, not_found} ->
      {false, Req4, State2};
    {ok, Value, Revision} ->
      {true, Req4, State2#state{value=Value, revision=Revision}}
  end.

generate_etag(Req, #state{revision=Revision}=State) ->
  Etag = integer_to_binary(Revision),
  {{strong, Etag}, Req, State}.


to_octet_stream(Req, #state{value=Value}=S) when is_binary(Value) ->
  {Value, Req, S};

to_octet_stream(Req, #state{value=Value}=S) when is_integer(Value) ->
  {integer_to_binary(Value), Req, S}.

from_octet_stream(Req, State) ->
  Conn = State#state.conn,
  Key = State#state.key,
  {ok, Body, Req2} = cowboy_req:body(Req),
  {Req3, Opts} = parse_if_match(Req2),
  {ok, Key, _Rev} = openkvs:put(Conn, Key, Body, Opts),
  {true, Req3, State}.

delete_resource(Req, State) ->
  Conn = State#state.conn,
  Key = State#state.key,
  {Req2, Opts} = parse_if_match(Req),
  {ok, Key} = openkvs:delete(Conn, Key, Opts),
  {true, Req2, State}.


parse_if_match(Req) ->
  case cowboy_req:parse_header(<<"if-match">>, Req) of
    {undefined, _, R} ->
      {R, []};
    {ok, undefined, R} ->
      {R, []};
    {ok, [{strong, Etag}], R} ->
      E = binary_to_list(Etag),
      Rev = list_to_integer(hd(string:tokens(E,"\""))),
      {R, [{db_version, Rev}]}
  end.
