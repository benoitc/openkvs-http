%% Copyright 2016, Bernard Notarianni
%%
%% Licensed under the Apache License, Version 2.0 (the "License"); you may not
%% use this file except in compliance with the License. You may obtain a copy of
%% the License at
%%
%%   http://www.apache.org/licenses/LICENSE-2.0
%%
%% Unless required by applicable law or agreed to in writing, software
%% distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
%% WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
%% License for the specific language governing permissions and limitations under
%% the License.

-module(openkvs_http_SUITE).

-export([all/0,
         end_per_suite/1,
         end_per_testcase/2,
         init_per_suite/1,
         init_per_testcase/2]).

-export([ basic_operations/1
        ]).

all() -> [ basic_operations
         ].

init_per_suite(Config) ->
  {ok, _} = application:ensure_all_started(openkvs_http),
  {ok, _} = application:ensure_all_started(hackney),
  Config.

init_per_testcase(TestCase, Config) ->
  {ok, _Pid, Uri} = openkvs:start_db(TestCase, [{backend, ets}]),
  {ok, Conn} = openkvs:connect(Uri),
  [{uri, Uri}, {conn, Conn}|Config].

end_per_testcase(TestCase, Config) ->
  Conn = proplists:get_value(conn, Config),
  ok = openkvs:close(Conn),
  ok = barrel_db:stop_db(TestCase),
  ok.

end_per_suite(Config) ->
  Config.

%%=======================================================================

basic_operations(Config) ->
  Uri = proplists:get_value(uri, Config),
  Conn = proplists:get_value(conn, Config),
  ConnHeaders = [{<<"Content-Type">>, <<"application/json">>}],

  %% connect
  {ok, <<"a">>, Rev_a} = openkvs:put(Conn, <<"a">>, 1, []),
  {200, AnswerHeaders, _} = req(post, "/_connect", #{uri => list_to_binary(Uri)}, ConnHeaders),
  Cookie = proplists:get_value(<<"set-cookie">>, AnswerHeaders),
  true = Cookie =/= undefined,
  Headers = [{<<"Cookie">>, Cookie},
             {<<"Content-Type">>, <<"application/octet-stream">>}],
  %% GET
  {200, H1, R1} = req(get, "/a", Headers),
  <<"1">> = R1,
  Rev_a = revision(H1),

  {404, _, _} = req(get, "/unknownkey", Headers),

  %% PUT new document
  {201, _, _} = req(put, "/b", <<"2">>, Headers),
  {ok, <<"2">>, Rev_b} = openkvs:get(Conn, <<"b">>),
  {200, H2, _r2} = req(get, "/b", Headers),
  Rev_b = revision(H2),

  %% PUT existing document, with revision in If-Match header
  %% cannot update value if bad revision is provided
  {412, _, _} = req(put, "/b", <<"3">>, [{<<"If-Match">>, <<"\"badrevision\"">>}|Headers]),
  {ok, <<"2">>, _} = openkvs:get(Conn, <<"b">>),

  %% value updated if last revision is provided
  Etag_Rev_b = etag(Rev_b),
  {204, _, _} = req(put, "/b", <<"3">>, [{<<"If-Match">>, Etag_Rev_b}|Headers]),
  {ok, <<"3">>, Rev_b2} = openkvs:get(Conn, <<"b">>),

  %% DELETE
  %% cannot delete value if bad revision is provided
  {412, _, _} = req(delete, "/b", <<"3">>, [{<<"If-Match">>, <<"\"badrevision\"">>}|Headers]),

  %% value updated if last revision is provided
  Etag_Rev_b2 = etag(Rev_b2),
  {204, _, _} = req(delete, "/b", <<"3">>, [{<<"If-Match">>, Etag_Rev_b2}|Headers]),

  %% GET keyrange
  {201, _, _} = req(put, "/c", <<"1">>, Headers),
  {200, _, J} = req(get, "/_keyrange/a/c", [],
                    [{<<"Cookie">>, Cookie},
                     {<<"Content-Type">>, <<"application/json">>}]),
  [Ra, Rc] = jsx:decode(J, [return_maps]),
  <<"a">> = maps:get(<<"key">>, Ra),
  1 = maps:get(<<"value">>, Ra),
  Rev_a = maps:get(<<"version">>, Ra),
  <<"c">> = maps:get(<<"key">>, Rc),

  ok.



revision(Headers) ->
  Etag = proplists:get_value(<<"etag">>, Headers),
  E = binary_to_list(Etag),
  list_to_integer(hd(string:tokens(E,"\""))).

etag(Revision) ->
  list_to_binary("\"" ++ integer_to_list(Revision) ++ "\"").

%%=======================================================================

req(Method, Route, Headers) ->
  req(Method,Route,[], Headers).

req(Method, Route, Map, Headers) when is_map(Map) ->
  Body = jsx:encode(Map),
  req(Method, Route, Body, Headers);

req(Method, Route, String, Headers) when is_list(String) ->
  Body = list_to_binary(String),
  req(Method, Route, Body, Headers);

req(Method, Route, Body, Headers) when is_binary(Body) ->
  Server = "http://localhost:8080",
  Path = list_to_binary(Server ++ Route),
  {ok, Code, RepHeaders, Ref} = hackney:request(Method, Path, Headers, Body, []),
  {ok, Answer} = hackney:body(Ref),
  {Code, RepHeaders, Answer}.
